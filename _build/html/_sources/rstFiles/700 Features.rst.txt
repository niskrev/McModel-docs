##################
Features
##################

.. topic:: Purpose:

	Here I test features that we may want to have in the documentation

	* equations_
	* tables_
	* `internal links`_
	* `external links`_
	* citations_

.. _equations:

Equations
==========

The the continuous forward rate R_ is:

.. math::
    R(t) & = \frac{1}{t}\int_{0}^{t}r(x)\partial x \\
         & = \frac{1}{t}\int_{0}^{t}\beta_{0} + \beta_{1} \cdot \exp(-\frac{x}{\tau})
           + \beta_{2} \cdot \frac{x}{\tau} \cdot \exp(-\frac{x}{\tau}) \partial x \\
         & = \frac{1}{t} \cdot \left(\beta_{0} \cdot t
           - \beta_{1} \cdot \tau \cdot \left(\exp(-\frac{t}{\tau}) - 1\right)
           + \beta_{2} \cdot \left( -t \cdot \exp(-\frac{t}{\tau})
           - \tau \cdot \left(\exp(-\frac{t}{\tau}) - 1\right)\right)\right) \\
         & = \beta_{0} + \beta_{1} \cdot \frac{1 - \exp(-\frac{t}{\tau})}{\frac{t}{\tau}}
           + \beta_{2} \cdot \left( \frac{1 - \exp(-\frac{t}{\tau})}{\frac{t}{\tau}} - \exp(-\frac{t}{\tau})\right)
   :label: NSformula

.. note::
    The derivation of the formula :eq:`NSformula` above is based on a continuous
    forward rate setup otherwise you couldn't talk about instantanious forward
    rate

Given a citation like [SW07]_, one
can also refer to it like SW07_.


.. _tables:

Tables
=======

+----------+--------------------------+
| variable | description              |
+==========+==========================+
| .. [R]   | continuous forward rate  |
+----------+--------------------------+



Internal links
===============
`Foreign block documentation <../_static/FB_documentation.pdf>`_


External links
===============
`FRB/US model: <https://www.federalreserve.gov/econres/us-models-about.htm>`_


.. _citations

Citations
=========

.. [SW07] Smets and Wouters (2007)
