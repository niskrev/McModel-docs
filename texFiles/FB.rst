.. role:: math(raw)
   :format: html latex
..

.. contents::
   :depth: 3
..

Foreign Block
=============

The foreign sector comprises overall 8 behavioural equations (total and
extra, volumes and prices). Where volume equations depend on foreign
(internal) demand and competitiveness indicators based on weighted
averages of indicators for the main trading partners.Trade prices depend
on costs, competitor prices, exchange rate and oil for import prices. It
is modelled as Error correction equations using a two step estimation
approach. First step estimation of long run relationship using fully
modified least squares supplemented by tests for the stationarity of the
residuals and allowing for cointegration tests imposing homogeneity.
Step two estimating the short run dynamics. In addition this sector
contains accounting identities to derive intra trade; the current
account balance and net foreign asset positions.

Exports and imports of goods and services
-----------------------------------------

Exports of goods and services, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Export volumes are estimated as ECM term
([eq:XTR\_long]-[eq:XTR\_short]). Demand elasticities are usually
constrained to 1 and the use of trends to catch structural changes in
export composition. We estimate cointegration in the long run while
assuming homogeneity between total exports and total foreign demand. The
long run structural changes in exports coming from other than price
competitiveness are modelled with use of trend. Meanwhile the short run
dynamics of intra and extra euro area exports must differ due to
different impact of exchange rate changes on relative prices on one
hand, and different contracts on the other. Therefore, we include
foreign demand intra and extra euro area separately into the short run
equation, and allow different impact of exchange rate and competitors’
prices on export volumes.

.. math::

   \begin{aligned}
     \label{eq:XTR_long}
     \log(XTR^{LR}) &=& \gamma_{0} + \log(WDR) + \gamma_{1} \log\left(XTD/CXD\right) + \gamma_{2} \cdot trend                                          \\
         \label{eq:XTR_short}
     \Delta\log(XTR) &=& \beta_{0} + \beta_{1} \Delta\log(XTR_{-1}) + \beta_{2} \Delta\log(WDR^{IN}) + \beta_{3} \Delta\log(WDR^{IN}_{-1}) + \nonumber \\
                 &+& \beta_{4} \Delta\log(WDR^{EX}) + \beta_{5} \Delta\log(WDR^{EX}_{-1}) + \beta_{6} \Delta\log(XTD) +                      \nonumber \\
                 &+& \beta_{7} \Delta\log(CXUD) + \beta_{8} \Delta\log(EXR) + \beta_{9} \Delta\log(EXR_{-1}) +                               \nonumber \\
                 &+& \beta_{10} \cdot \left(\log(XTR_{-1}) - \log(XTR^{LR}_{-1})\right) + \epsilon^X\end{aligned}

where XTR is total real exports, WDR is real world demand for export
goods of the country, which is divided into world demand from intra and
extra euro area (WDRIN and WDREX, respecitively). The relative prices
are determined as the share of export deflator (XTD) to the competitors’
prices in euro (CXD) which is calculated from the competitors’ prices in
USD (CXUD) using USD exchange rate (EXR).

The estimates for each country are the following.

**Germany:**

.. math::

   \begin{aligned}
     \label{eq:XTR_long_de}
     \log(XTR^{LR}) &=& 13.48^{***} + \log(WDR) - 0.24^{**} \log\left(XTD/CXD\right) +                             \nonumber \\
                 &+& 0.0010^{***} \cdot trend                                                                                \\
     \label{eq:XTR_short_de}
     \Delta\log(XTR) &=& 0.002^{} + 0.67^{***} \Delta\log(WDR^{IN}) + 0.32^{**} \Delta\log(WDR^{EX}) +             \nonumber \\
                 &+& 0.30^{***} \Delta\log(CXUD) + 0.25^{***} \Delta\log(EXR) +                                    \nonumber \\
                 &-& 0.29^{***} \cdot \left(\log(XTR_{-1}) - \log(XTR^{LR}_{-1})\right) + \epsilon^{X,DE}\end{aligned}

**France:**

.. math::

   \begin{aligned}
     \label{eq:XTR_long_fr}
     \log(XTR^{LR}) &=& 15.43^{***} + \log(WDR) - 1.09^{***} \log\left(XTD/CXD\right) +                            \nonumber \\
                 &-& 0.0055^{***} \cdot trend - 0.250^{***} \cdot D^{09} + 0.0037^{***} \cdot D^{09} \cdot trend             \\
     \label{eq:XTR_short_fr}
     \Delta\log(XTR) &=& -0.002^{} + 0.50^{***} \Delta\log(WDR^{IN}) + 0.44^{***} \Delta\log(WDR^{EX}) +           \nonumber \\
                 &+& 0.11^{***} \Delta\log(EXR_{-1}) +                                                             \nonumber \\
                 &-& 0.08^{} \cdot \left(\log(XTR_{-1}) - \log(XTR^{LR}_{-1})\right) + \epsilon^{X,FR}\end{aligned}

**Italy:**

.. math::

   \begin{aligned}
     \label{eq:XTR_long_it}
     \log(XTR^{LR}) &=& 10.89^{***} + \log(WDR) - 0.42^{***} \log\left(XTD/CXD\right) +                            \nonumber \\
                 &-& 0.0075^{***} \cdot trend - 0.418^{***} \cdot D^{09} + 0.0062^{***} \cdot D^{09} \cdot trend             \\
     \label{eq:XTR_short_it}
     \Delta\log(XTR) &=& -0.007^{***} + 0.59^{***} \Delta\log(WDR^{IN}) + 0.52^{***} \Delta\log(WDR^{EX}) +        \nonumber \\
                 &-& 0.59^{***} \Delta\log(XTD) + 0.48^{***} \Delta\log(CXUD) + 0.40^{***} \Delta\log(EXR) +       \nonumber \\
                 &-& 0.23^{**} \cdot \left(\log(XTR_{-1}) - \log(XTR^{LR}_{-1})\right) + \epsilon^{X,IT}\end{aligned}

**Spain:**

.. math::

   \begin{aligned}
     \label{eq:XTR_long_es}
     \log(XTR^{LR}) &=& 11.29^{***} + 1.38^{***} \log(WDR) - 0.74^{**} \log\left(XTD/CXD\right) +                  \nonumber \\
                 &-& 0.0043^{**} \cdot trend + 0.283^{**} \cdot D^{0408} - 0.0070^{***} \cdot D^{0408} \cdot trend           \\
     \label{eq:XTR_short_es}
     \Delta\log(XTR) &=& 0.002^{} + 0.55^{***} \Delta\log(WDR^{IN}) + 0.75^{***} \Delta\log(WDR^{EX}) +            \nonumber \\
                 &-& 0.30^{***} \Delta\log(WDR^{EX}_{-1}) - 0.52^{***} \Delta\log(XTD) +                           \nonumber \\
                 &-& 0.20^{***} \cdot \left(\log(XTR_{-1}) - \log(XTR^{LR}_{-1})\right) + \epsilon^{X,ES}\end{aligned}

**Netherlands:**

.. math::

   \begin{aligned}
     \label{eq:XTR_long_nl}
     \log(XTR^{LR}) &=& 14.30^{***} + \log(WDR) - 0.64^{***} \log\left(XTD/CXD\right) +                            \nonumber \\
                 &-& 0.0007^{***} \cdot trend                                                                                \\
     \label{eq:XTR_short_nl}
     \Delta\log(XTR) &=& 0.001^{} + 0.59^{***} \Delta\log(WDR^{IN}) + 0.33^{**} \Delta\log(WDR^{EX}) +             \nonumber \\
                 &-& 0.54^{***} \Delta\log(XTD) + 0.49^{**} \Delta\log(CXUD) + 0.42^{***} \Delta\log(EXR) +        \nonumber \\
                 &-& 0.36^{***} \cdot \left(\log(XTR_{-1}) - \log(XTR^{LR}_{-1})\right) + \epsilon^{X,NL}\end{aligned}

Imports of goods and services, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Exports of goods and services, extra euro area, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Imports of goods and services, extra euro area, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Exports of goods and services, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Imports of goods and services, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Exports of goods and services, extra euro area, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Imports of goods and services, extra euro area, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional identities
---------------------

Price of Oil
~~~~~~~~~~~~

Price of Energy
~~~~~~~~~~~~~~~

Current account and net foreign assets
--------------------------------------

Current account
~~~~~~~~~~~~~~~

Net foreign assets
~~~~~~~~~~~~~~~~~~
