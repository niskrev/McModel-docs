\section{The model}
The model is a two country model. The first country is defined as an individual country of the euro area, while the second country is constructed as the rest of the euro area (REA). The cross country relations between these two entities are kept simple and comprise, the joint determination of interest rates and an expenditure shifting argument leading to an influence of relative prices between the two countries on the demand for domestically produced goods in the individual country and in the REA. This expenditure shifting argument allows to ensure stability, in a system that cannot be stabilized by the euro area interest rate alone.\footnote{Compare Gali and Monacelli (2008) on stability in a currency union setting, see 'Optimal Monetary and Fiscal Policy in a Currency Union', Journal of International Economics, vol. 76, 2008, 116-132} To keep notation simple, all variables without a superscript are the individual euro area country while a star refers to the REA and joint variables of both countries are superscripted by 'EA'.\footnote{To reduce the complexity of the model, the modelling of the country specific REAs is simplified to contain only an IS and a price Phillips curve }
\subsection{Output}

The output gap is modelled via a reduced form IS curve:

\begin{eqnarray}
\hat{y}_{t}=\beta _{\hat{y},+1}^{\hat{y}}E_t \hat{y}_{t+1}+\beta _{\hat{y},-1}^{\hat{y}} \hat{y}_{t-1}
+\beta _{\Delta \hat{y},\star}^{\hat{y}} \Delta \hat{y}^{\star}  -\beta _{r\pi}^{\hat{y}} \left( \hat{\pi}_t-\hat{\pi}_t^{\star}  \right)     -\beta _{\hat{r}}^{\hat{y}%
}\hat{r}_{t}+e_{\hat{y},t}
\end{eqnarray}%

where $\hat{y}_{t}^{ }$ is the log deviation of output from its trend, $\hat{r}_{t}^{ }$ is the real interest rate gap, $\hat{\pi}_t$ and $\hat{\pi}_t^{\star}$ denotes domestic and foreign inflation,$\Delta \hat{y}^{\star}$ the change in foreign output gap  and $e_{%
\hat{y},t}^{ }$ is an error term. The structure of this equation follows the general structure of an IS curve under a form of real rigidities like habit formation in consumption. The gap between foreign and domestic inflation is introduced to approximate an expenditure switching argument. If domestic prices accelerate at a higher pace than foreign prices, the domestic economic is experiencing a reduction in price competitiveness and a lower demand for domestically produced goods. The change in foreign output proxies for the foreign demand for domestic products.
Trend growth ($\Delta \bar{y}_{t}^{ }$) is modelled via an AR process
\begin{eqnarray}
\Delta \bar{y}_{t}=\left( 1-\rho _{\Delta \bar{y}}\right) \Delta \bar{y}%
_{ss}+\rho _{\Delta \bar{y}}\Delta \bar{y}_{t-1}+e_{\Delta \bar{y},t}
\end{eqnarray}%

So that observed output growth can be matched to the model variables via the following measurement equation:
\begin{eqnarray}
\Delta y_{t}=\hat{y}_{t}-\hat{y}_{t-1}+\Delta \bar{y}_{t}
\end{eqnarray}%


\subsection{Inflation}

 Price inflation is modelled via a reduced form NK Phillips Curve, where inflation is modelled as a deviation from a time-varying inflation attractor ($\bar{\pi}$). The equation shows that actual inflation depends both on a measure of expected inflation and past inflation, the deviation of output from trend and wage inflation.\footnote{The inclusion of wage inflation as an additional variable in the inflation equation is producing stability in WAPRO. The introduction of this term will re-evaluated once the equation is exported to the ECB-MC. }
\begin{eqnarray}
\hat{\pi }_{t}=\beta _{\hat{\pi} }\hat{\pi} _{4,t+4}+(1-\beta _{\hat{\pi} })\hat{\pi} _{4,t-1}+\beta _{\hat{y}}^{\hat{\pi }}\hat{y}_{t-1}+\beta _{\hat{\pi}^{w}}^{\hat{\pi }}\hat{\pi}^{w}_{4,t}+e_{\hat{\pi} ,t}
\end{eqnarray}%

To improve the fit the lagged inflation variable is the annual inflation rate



\begin{eqnarray}
\hat{\pi} _{4,t}=\frac{1}{4}\left( \hat{\pi} _{t}+\hat{\pi} _{t-1}+\hat{\pi} _{t-2}+\hat{\pi}
_{t-3}\right)
\end{eqnarray}%




Note that inflation is modelled with reference to a country specific inflation attractor $\bar{\pi}$. The euro area inflation target is modelled in the Taylor rule for the euro area. The average inflation in the individual countries of the euro area deviates substantially from the euro area inflation target. To model this discrepancy the country specific inflation attractors are introduced as follows:
\begin{eqnarray}
\bar{\pi}_{t}=(1-\beta_{\bar{\pi}})\pi^{EA,\ast}_{ss,ss}+\beta_{\bar{\pi}}\bar{\pi}_{t-1}+\beta^{\bar{\pi}}_{\hat{\pi}_{4}}\hat{\pi}_{4,t}+e_{\bar{\pi} ,t}
 \end{eqnarray}%
Inherent to this specification is the property that repeated low inflation outcomes will drive down the country specific inflation attractor temporarily.
Finally, the level of inflation is computed using the inflation gap and the inflation attractor:
\begin{eqnarray}
\hat{\pi}_{t}=\pi_{t}-0.25 \bar{\pi}_{t}
 \end{eqnarray}%





\subsection{Wage Inflation}

The wage inflation gap is modelled via a reduced form NK Phillips wage curve. The equation shows that actual wage inflation gap depends on future wage inflation and past domestic price inflation, the deviation of trend output growth from its steady state and the change in the unemployment gap.




\begin{eqnarray}
\hat{\pi}_{t}^{w}=\begin{array}{c} \beta_{\hat{\pi}^{w}}\hat{\pi}_{t+1}^{w}++\xi_{\hat{\pi}^{w}}\hat{\pi}_{t-1}
+\beta_{\Delta \bar{y}}^{\hat{\pi}^{w}}\left(\Delta \bar{y}_{t}-\Delta \bar{y}_{ss} \right)-\beta_{u}^{\hat{\pi}^{w}}\left(\hat{u}_{t}-\hat{u}_{t-1} \right)-e_{\hat{\pi}_{t}^{w},t}
\end{array}
\end{eqnarray}%

\begin{eqnarray}
\pi_{4,t}^{w}=\hat{\pi}^{w} _{4,t}+{\pi}_{4,t}+\Delta \bar{y}_{t}
\end{eqnarray}%







\subsection{Unemployment}
The unemployment gap ($\hat{u}$) is defined as the difference between actual unemployment rate and the natural level of the unemployment rate ($\bar{u}$): $\hat{u}_{t}=u_{t}-\bar{u}_{t}$

 The unemployment gap follows a flexible interpretation of Ocun's law and is positively related to its level in $t-1$ and  it is negatively related to the output gap.
\begin{eqnarray}
\hat{u}_{t}=\rho _{\hat{u}}\hat{u}_{t-1}+\beta_{\hat{y}}^{\hat{u}}\hat{y}_{t}+e_{\hat{u},t}
 \end{eqnarray}%

The natural level of the unemployment rate follows an autoregressive structure in it's own level and the growth rate of natural unemployment ($GU_t$) which is modelled as an AR(1) process.\footnote{In the estimation of the model, this equation is extended for some countries by the lagged value of unemployment as a driver of natural unemployment. THis is done to bring the model implied NAIRU closer to the official NCB measures of the NAIRU.}
\begin{eqnarray}
\bar{u}_{t}=\left( 1-\rho _{\bar{u}}\right)  \bar{u}_{t-1}+\rho _{ \bar{u}} \bar{u}_{ss}+GU_{t} +e_{\bar{u},t}
 \end{eqnarray}%

\begin{eqnarray}
GU_{t}=\left(1-\rho _{GU}\right)GU_{t-1}+e_{GU,t}
 \end{eqnarray}%




\subsection{Interest Rates}
The real interest rate for the individual country is given by the difference between the nominal interest rate for the euro area and the expected domestic price inflation rate.
\begin{eqnarray}
r_{t}=i_{t}^{EA}-E_t\pi _{t+1}
\end{eqnarray}%
$\hat{r}_{t}^{ }$ is the log deviation of the real interest rate from its trend.
\begin{eqnarray}
\hat{r}_{t}=r_{t}-\bar{r}_{t}^{EA}
\end{eqnarray}%






\subsection{Policy Rule}

 The monetary authority sets the nominal interest rate according to  a policy rule which contains its value in the previous period,the nominal interest rate trend, a weighted average of home and foreign inflation and a weighted average of home and foreign output gaps.
\begin{eqnarray}
i_{t}^{EA}=\rho_{i,1}^{EA}i_{t-1}^{EA}+\left( 1-\rho_{1,1}^{EA}\right) \left\{
\begin{array}{c}
\bar{r}_{t}^{EA}+\pi _{t}^{\ast,EA} \\
+\rho_{i,2}^{EA}\left[ \omega_{t, \pi }4 \pi _{t}+\left( 1-\omega_{t, \pi }\right)4\pi
_{t}^{\star }-\pi _{t}^{\ast ,EA}\right]  \\
+\rho_{i,3}^{EA}\left[ \omega_{t, y}\hat{y}_{t}+\left( 1-\omega_{t, y}\right) \hat{y}_{t}^{\star }%
\right]
\end{array}%
\right\} +e_{i,t}^{EA}
\end{eqnarray}%

where the euro area inflation target is modelled as an AR process.
\begin{equation}
\pi_{t}^{\ast ,EA}=\rho _{\pi ^{\star }}^{EA}\pi _{t-1}^{\ast ,EA}+\left(
1-\rho _{\pi ^{\star }}^{EA}-\rho_{\bar{\pi} }^{EA}\right) \pi _{ss,ss}^{\ast ,EA}+
 \rho_{\bar{\pi} }^{EA}\left(\omega_{\pi }\pi_{4,t-1}+(1-\omega_{\pi })\pi _{4,t-1}^{ \star}  \right)+e_{\pi^{\ast},t}^{EA}
\end{equation}%

\begin{equation}
\pi_{4,t}^{EA}=\omega_{t, \pi}\pi_{4,t}+\left( 1-\omega_{t, \pi }\right) \pi_{4,t}^{,\star }
\end{equation}%

\begin{eqnarray}
\bar{r}_{t}^{EA}=\rho _{\bar{r}}^{EA}\bar{r} _{t-1}^{EA}+\left(
1-\rho _{\bar{r}}^{EA}\right) \bar{r} _{ss}^{EA}+e_{\bar{r},t}^{EA}
\end{eqnarray}%

The parameters $\omega_{t,i}$ give the wights of aggregating the output gap and inflation back to the euro area figures

\begin{eqnarray}
gap_{t}^{EA}=\omega_{t, 8y }\hat{y}_{t}+\left( 1-\omega_{t, y }\right) \hat{y}_{t}^{\star }
\end{eqnarray}%
\subsection{Rest of the euro area}

The rest of the euro area (REA) is defined as the euro area less the country modelled in the first block, and is therefore country-specific. To reduce complexity the REA is modelled only via the IS and the Phillips curve, the only two equations necessary to complete the system with the area wide determination of the interest rates.

\section{Data and estimation}

The model is estiamted on the following variables:
\begin{enumerate}
\item GDP deflator (individual country and REA)
\item GDP growth (individual country and REA)
\item unemployment (individual country)
\item growth rate of compensation per private employee (individual country)
\item euro area interest rate
\end{enumerate}

The first two variables are taken twice. Once for Germany (DE) and once for the rest of the euro area (REA). We  assume fixed weights for the individual country via the REA. The sample is 2000q1 to 2015q3.

A somewhat different approach was taken for Italy. Using the GDP deflator as the central pricing information resulted in a deterioration of model properties, in comparison to the other countries.\footnote{Especially the forecasting properties were disappointing.} To address this problem we follow Justiniano et. al.(2013) \footnote{ see A. Justitiano, G. E. Primiceri, and A. Tambalotti,'Is There a Trade-Off Between Inflation and Output Stabilization?', American Economic Journal: Macroeconomics, 5(2), April 2013, pp. 1-31.} and introduce an additional inflation series (HICP excluding energy) and define a small filtering system inside WAPRO.\footnote{More specifically, both observable inflation series are linked to the same model concept of inflation plus a measurement error for each equation: \begin{eqnarray*}
\pi_t^{4,yed}=\pi_t^{4,M}+\epsilon_t^{ME,yed}\\
\pi_t^{4,hex}=\pi_t^{4,M}+\epsilon_t^{ME,hex}
\end{eqnarray*}, where $M$ stands for the model based inflation, $YED$ for the GDP deflator and $HEX$ for HICP without energy. $ME$ stands for  measurement error. The idea of this approach is to define the two observed series as imperfectly, but driven by a joint component. The two measurement errors are estimated by the Kalman filter to identify the joint inflation driver}
