# This project contains the web-based documentation of the Multicountry model.

* The source files are located in [rstFiles](./rstFiles).

* The rstFiles are produced by pandoc using the tex files in folder [texFiles](./texFiles).
  e.g. to produce fname.rst from fname.tex I run

`pandoc -s -t rst --toc fname.tex -o fname.rst`

* Some manual editing of the ooutput files are necessary, such as
 - remove the LATEX related rst things on the top of the file::

```
    .. role:: math(raw)
      :format: html latex
    ..

    .. contents::
      :depth: 3
    ..
```

 - Remove indentation of the line after all equations.


* Equations should be labeled like this:

```
    .. math::
      :label: MND

        \ln(XTD) = \alpha_{0} + \alpha_{xtd} \ln(XXD) + (1-\alpha_{xtd}) \ln(XND)
```

* \hat is distorted in the html output, use \widehat instead

### build html file with

```
make html
```
### open html file with

```
open _build/html/index.html
```

## Requirements:
* [sphinx](http://www.sphinx-doc.org/en/stable/install.html)
* [pandoc](https://pandoc.org/)


