Household Wealth
===============================================

Household sector wealth is defined as follows

.. math:: HHW_{t}=GHW_{t}+NFW_{t}  \label{HHW}

where :math:`GHW_{t}` is the gross housing wealth :math:`NFW_{t}` is
net financial wealth

The stock of **nominal housing wealth** is revalued in line with changes
in house prices (:math:`HP_{t}`)

.. math::
   :label: GHW

   GHW_{t} = IH_{t} + (1-Depr)IH_{t-1}(HP_{t}/HP_{t-1})

where :math:`IH_{t}` is Nominal Residential Investment (gross capital
formation, household sector)  [1]_ ; :math:`HP_{t}` is residential
property prices (New and existing dwellings; Residential property in
good and poor condition) and :math:`\textit{Depr}` is the depreciation
rate for housing stock and it is assumed to be equal to YYY. Land and
other non-modelled components are ignored.The discrepancy between the
model implied and actual Housing wealth (:math:`QGHW_{t}`\ =
:math:`GHW_{t,model}`/ :math:`GHW_{t,data}`) is calculated and used in
simulations as correction factor.

Household net financial wealth (:math:`NFW_{t}`) is modelled as follows

.. math::
   :label: NFW

   NFW_{t} = (Y_{t} -C_{t}- IH_{t}) +NFW_{t-1}REV_{t}

where :math:`Y_{t}` is the Nominal Disposable Income, :math:`C_{t}` is
the Nominal (total) Consumption, :math:`IH_{t}` is the Nominal
Residential Investment (gross fixed capital formation, dwellings) and
:math:`REV_{t}` is the Revaluation Term.

Net financial wealth includes several types of assets (treasury bonds,
equities, deposits, foreign assets, other debt securities …) that need
to be re-valuated with the changes in different asset prices. The
revaluation term will be a weighted average of the change in the
corresponding asset prices (equity prices and debt securities) with the
weights reflecting the relative importance of the various components in
the asset holding of households:

.. math:: REV_{t}=c+a^{GB} (R_{t-1}^{GB}/R_{t}^{GB}) +a^{CB} (R_{t-1}^{CB}/R_{t}^{CB}) + a^{EQ} (EQP_{t}/EQP_{t-1})

where :math:`c` is the fraction of assets not subject to revaluation
(mostly deposits with banks), :math:`a^{GB}` is the fraction of
government debt securities (held directly and indirectly by household)
in total financial assets, :math:`a^{CB}` is the fraction of corporate
debt securities (held directly and indirectly by household) in total
financial assets and :math:`a^{EQ}` is the fraction of equities (listed
Equities + half of the unlisted equities held by households) in total
financial assets, whereas :math:`R_{t}^{GB}`\ is the domestic long-term
bond yield (10YR), :math:`R_{t}^{CB}` is the domestic corporate bond
yield and :math:`EQP_{t}` is the domestic stock market price index.

+----+--------+--------------------+--------------------+------------------------+
|    | c      |   :math:`a^{LT}`   |  :math:`a^{CB}`    |   :math:`a^{EQP}`      |
+====+========+====================+====================+========================+
| DE | 67.8   | 2                  | 16.3               | 13.9                   |
+----+--------+--------------------+--------------------+------------------------+
| ES | 51.7   | 6.5                | 22.1               | 19.7                   |
+----+--------+--------------------+--------------------+------------------------+
| FR | 53.3   | 14.8               | 16.8               | 15.2                   |
+----+--------+--------------------+--------------------+------------------------+
| IT | 66.8   | 6.1                | 7.8                | 19.3                   |
+----+--------+--------------------+--------------------+------------------------+
| NL | 57     | 3.9                | 18.1               | 21                     |
+----+--------+--------------------+--------------------+------------------------+

.. [1]
   See Appex...For an alternative formulation that uses gross fixed
   capital formation, dwellings, total economy
