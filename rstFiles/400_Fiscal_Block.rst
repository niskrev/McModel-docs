################
Fiscal block
################

.. topic:: Introduction:

	Here goes an introduction to the fiscal block


************
Sub-block 1
************

Something about sub-block 1

************
Sub-block 2
************

Something about sub-block 2

:download: `Fiscal block documentation <../_static/Fiscal_Block_documentation.pdf>`_
